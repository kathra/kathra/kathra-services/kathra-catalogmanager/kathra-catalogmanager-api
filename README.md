# Kathra Catalog Manager API

This API provides management of the Kathra components into repository catalog. 

Main features insured by CatalogManager are : 
 - Generate packages from template
 - Push packages into repository from :
    - package generated with templates
    - archives files
    - sources repositories for custom needs
 - Get entries availables from repositories
 - Use this entries to install theses packages into Kathra RuntimeManager




To consult OpenApi specs, 
import specs from https://gitlab.com/kathra/kathra/kathra-services/kathra-catalogmanager/kathra-catalogmanager-api/raw/1.1.x/swagger.yaml into https://editor.swagger.io/


POC :
https://gitlab.com/kathra/kathra/kathra-services/kathra-catalogmanager/kathra-catalogmanager-go/kathra-catalogmanager-helm

This POC implements Helm Chart packages

```mermaid
graph LR
User -- 1. Generate component --> KC
KC[Kathra Core] -- 2. Configure and submit template --> CM[Catalog Manager]
CM -- 3. Generate package --> CM
CM -- 4. Publish package --> KR((Kathra catalog repository))
User -- 5. Search packages avaliables --> CM
CM -- 6. Pull Kathra packages --> KR
CM -- 6. Pull others packages officials --> OR((Other repositories))
CM -- 7. Provide existing packages with its arguments --> User
User -- 8. Deploy package configured --> RM
RM[Runtime Manager] -- 9. Pull package --> KR
RM -- 9. Pull --> OR
RM -- 10. Deploy and run --> Runtime Host